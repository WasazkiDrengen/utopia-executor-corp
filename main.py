import discord
from discord.ext import commands

import asyncio
import traceback
import sys
import os
import datetime

import cogs.data.database as database
import cogs.data.config as config
import cogs.data.checks as checks

async def get_prefix(bot, message):
    prefix = await database.get_prefix(message.guild.id)
    return [prefix, "u!", "U!"]


#version 1.0
description = "The Utopia Executor for Utopia City"
bot = commands.Bot(command_prefix=get_prefix, description=description)
bot.remove_command('help')

startup_cogs = ['cogs.mod', 'cogs.info', 'cogs.animals', 'cogs.fun', 'cogs.gamenight', 'cogs.join', 'cogs.help', 'cogs.points']


@bot.event
async def on_ready():
    global uptime_time
    uptime_time = datetime.datetime.utcnow()
    print("{} is up and running.".format(bot.user.display_name))
    print("With ID {}.".format(bot.user.id))
    bot.loop.create_task(play())


async def play():
    while True:
        await bot.change_presence(status=discord.Status.online,
                            activity=discord.Game("Watching over Utopia City"))
        await asyncio.sleep(14)

        await bot.change_presence(status=discord.Status.online,
                            activity=discord.Game("Call me by u!"))
        await asyncio.sleep(15)

        await bot.change_presence(status=discord.Status.idle,
                            activity=discord.Game("Taking a sip of coffee."))
        await asyncio.sleep(8)

        await bot.change_presence(status=discord.Status.online,
                            activity=discord.Game("Hot!"))
        await asyncio.sleep(5)

        await bot.change_presence(status=discord.Status.idle,
                            activity=discord.Game("Searching for criminals"))
        await asyncio.sleep(20)

        await bot.change_presence(status=discord.Status.idle,
                            activity=discord.Game("Cleaning chats"))
        await asyncio.sleep(18)

@bot.command()
@commands.check(checks.utopia_check)
async def uptime(ctx):
    delta = datetime.datetime.utcnow() - uptime_time
    total_seconds = deta.total_seconds()
    days = total_seconds // 86400
    hours = total_seconds // 3600
    minutes = (total_seconds // 3600) // 60
    seconds = seconds % 60
    embed = discord.Embed(title="Uptime", description="The number of seconds, minutes, hours and days the bot have been up for.", colour=discord.Colour.blue())
    embed.add_field(name="Days", value=days)
    embed.add_field(name="Hours", value=hours)
    embed.add_field(name="Minutes", value=minutes)
    embed.add_field(name="Seconds", value=seconds)
    embed.add_field(name="Total Seconds", value=total_seconds, inline=False)
    await ctx.send(embed=embed)



@bot.command()
@commands.check(checks.dev_check)
async def load(ctx, cog: str = None):

    if cog == 'all':
        cogs = os.listdir('cogs/')
        for cog in cogs:
            if cog[-3:] != '.py':
                continue
            cog = 'cogs.' + cog.lower()[:-3]

            try:
                bot.load_extension(cog)

            except Exception as e:
                await ctx.send("An error occured while trying to load {},\n{}".format(cog, e))
                print('A error occurred in {}.'.format(cog), file=sys.stderr)
                traceback.print_exc()

            else:
                await ctx.send("Succesfully loaded {}".format(cog))

    else:
        cog = 'cogs.' + cog.lower()
        try:
            bot.load_extension(cog)
        except Exception as e:
            await ctx.send("An error occured while trying to load {},\n{}".format(cog, e))
            print('A error occurred in {}.'.format(cog), file=sys.stderr)
            traceback.print_exc()
        else:
            await ctx.send("Succesfully loaded {}".format(cog))


@bot.command()
@commands.check(checks.dev_check)
async def full_load(ctx, *, exceptions: str = None):
    if exceptions is not None:
        exceptions = exceptions.split(" ")
    if exceptions is None:
        exceptions = []
    cog_files = os.listdir('cogs/')
    loaded_cogs = bot.cogs
    cog_files_ok = []
    good_loaded_cogs = []
    bad_loaded_cogs = []
    formatted_exceptions = []
    for to_format in exceptions:
        if to_format.endswith(".py"):
            to_format = to_format[:-3]
        to_format = to_format.lower()
        formatted_exceptions.append(to_format)

    for cog in loaded_cogs:

        if cog.startswith("cogs."):
            cog = cog.replace("cogs.", "")
        cog = cog.lower()
        cog = cog.replace(".py", "")
        if cog not in formatted_exceptions:
            good_loaded_cogs.append(cog)
        else:
            bad_loaded_cogs.append(cog)

    for file in cog_files:
        if str(file).endswith('.py') and str(file) not in formatted_exceptions and str(file) not in good_loaded_cogs:
            cog_files_ok.append(file[:-3])

    for cog_to_load in cog_files_ok:
        try:
            bot.load_extension('cogs.'+ str(cog_to_load).lower())
        except Exception as e:
            await ctx.send("An error occured while trying to load {},\n{}".format(cog, e))
            print('A error occurred in {}.'.format(cog), file=sys.stderr)
            traceback.print_exc()

    for cog_to_unload in bad_loaded_cogs:
        try:
            bot.unload_extension('cogs.' + str(cog_to_unload).lower())
        except Exception as e:
            await ctx.send("An error occured while trying to unload an exception ({}),\n{}".format(cog, e))
            print('A error occurred in {}.'.format(cog), file=sys.stderr)
            traceback.print_exc()
    await cogsbis(ctx)

async def cogsbis(ctx):
    embed = discord.Embed(title="Task done!", description="Here's all the loaded cogs.", color=discord.Colour.blue())
    cog_files = os.listdir('cogs/')
    loaded_cogs = bot.cogs
    cog_files_py = []
    for file in cog_files:
        if str(file).endswith('.py'):
            cog_files_py.append(str(file)[0].upper() + str(file)[1:-3])

    for cog_file in cog_files_py:
        if cog_file in loaded_cogs:
            embed.add_field(name=cog_file, value="True")
        else:
            embed.add_field(name=cog_file, value="False")

    await ctx.send(embed=embed)


@bot.command()
@commands.check(checks.dev_check)
async def unload(ctx, cog: str = None):
    cog = 'cogs.' + cog.lower()
    try:
        bot.unload_extension(cog)
    except Exception as e:
        await ctx.send("An error occured while trying to unload {},\n{}".format(cog, e))
        print('A error occurred in {}.'.format(cog), file=sys.stderr)
        traceback.print_exc()
    else:
        await ctx.send("Succesfully unloaded {}".format(cog))


@bot.command()
@commands.check(checks.dev_check)
async def reload(ctx, cog: str = None):

    if cog == 'all':
        cogs = os.listdir('cogs/')
        for cog in cogs:
            if cog[-3:] != '.py':
                continue
            cog = 'cogs.' + cog.lower()[:-3]

            try:
                bot.unload_extension(cog)
                bot.load_extension(cog)

            except Exception as e:
                await ctx.send("An error occured while trying to load {},\n{}".format(cog, e))
                print('A error occurred in {}.'.format(cog), file=sys.stderr)
                traceback.print_exc()

            else:
                await ctx.send("Succesfully reloaded {}".format(cog))
    else:
        cog = 'cogs.' + cog.lower()
        try:
            message = await ctx.send("Reloading...")
            bot.unload_extension(cog)
            bot.load_extension(cog)
        except Exception as e:
            await ctx.send("An error occured while trying to reload {},\n{}".format(cog, e))
            print('A error occurred in {}.'.format(cog), file=sys.stderr)
            traceback.print_exc()


@bot.command()
@commands.check(checks.dev_check)
async def loaded(ctx):
    embed = discord.Embed(title="Loaded Cogs", description="Here's all the loaded cogs.", color=discord.Colour.blue())
    cog_files = os.listdir('cogs/')
    loaded_cogs = bot.cogs
    cog_files_py = []

    for file in cog_files:
        if str(file).endswith('.py'):
            cog_files_py.append(str(file)[0].upper() + str(file)[1:-3])

    for cog_file in cog_files_py:
        if cog_file in loaded_cogs:
            embed.add_field(name=cog_file, value="True")
        else:
            embed.add_field(name=cog_file, value="False")

    await ctx.send(embed=embed)

@bot.command()
@commands.check(checks.dev_check)
async def shutdown(ctx):
    await bot.say("Shutting down.....\nBye")
    await bot.logout()


if __name__ == '__main__':
    for cog in startup_cogs:
        try:
            bot.load_extension(cog)
        except Exception as e:
            print('A error occurred in {}.'.format(cog), file=sys.stderr)
            traceback.print_exc()


bot.run(config.token)

import discord
from discord.ext import commands

import requests
import json
import random

import cogs.data.checks as checks


class Fun:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="8ball")
    @commands.check(checks.utopia_check)
    async def _ball(self, ctx, *, question: str = "Did i forget to add a question?"):
        answers = {"Yeah": 0x2ecc71, "Yes": 0x2ecc71, "Why not?": 0x2ecc71, "Yup": 0x2ecc71, "What could go wrong?": 0x2ecc71,
                    "Why not?": 0x2ecc71, "Go for it": 0x2ecc71, "True": 0x2ecc71, "Tru": 0x2ecc71, "Correct": 0x2ecc71, "Right": 0x2ecc71,
                    "Pretty sure, yes..": 0x1f8b4c, "Maybe": 0x1f8b4c, "Ehh, think so..": 0x1f8b4c, "Hope so...": 0x1f8b4c, "I don't know..": 0x1f8b4c,
                    "Ask again later!": 0xe67e22, "Don't know": 0xe67e22, "Why are you asking me this?": 0xe67e22, "Ask god...": 0xe67e22, "Im not smart enough to answer that!": 0xe67e22,
                    "Why did you ask?": 0xe67e22, "I don't know how to answer that!": 0xe67e22,
                    "Pay me and i'll answer..": 0xa84300, "Got money?": 0xa84300, "You know, i don't do this for free..": 0xa84300, "Why should i answer that?": 0xa84300,
                    "Hope not": 0xe74c3c, "Ehh, don't think so..": 0xe74c3c, "Not sure": 0xe74c3c, "Not really!": 0xe74c3c, "Very doubtful.": 0xe74c3c,
                    "Nope": 0x992d22, "Don't": 0x992d22, "No": 0x992d22, "Nah!": 0x992d22, "Hell na'": 0x992d22, "Hell no!": 0x992d22}
        answer = random.choice(list(answers.keys()))
        embed = discord.Embed(title=question, description=answer, colour=answers[answer])
        await ctx.send(embed=embed)
        
    @commands.command()
    @commands.check(checks.commands_check)
    async def gtn(self, ctx):
    
        def check(message):
            if ctx.channel.id == message.channel.id and ctx.message.author.id == message.author.id: return True
        
        number = random.randint(1, 100)
        tries = 0
        #for extra security
        run = True

        await ctx.send("Guess a number")
    
        try:
            while run:
                guess = int((await self.bot.wait_for("message", check=check)).content)
                tries += 1
            
                if guess < number:
                await ctx.send("Guess higher!")

                elif guess > number:
               await ctx.send("Guess lower!")

                elif guess == number:
                    await ctx.send("Correct! It took {} tries.".format(tries))
                    #security reasons
                    run = False
                    break
            
        except asyncio.TimeoutError:
            await ctx.send("You took too long to answer, try again!")
        
    @commands.command()
    @commands.check(checks.special_utopia_check)
    async def urban(self, ctx, *, word: str = "urban"):
        data = (requests.get("http://api.urbandictionary.com/v0/define?term=" + word)).json()["list"][2]
        embed = discord.Embed(title=word[0].upper() + word[1:], description=data["definition"])
        embed.add_field(name="Example", value=data["example"])
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Fun(bot))

def teardown(bot):
    bot.remove_cog('Fun')

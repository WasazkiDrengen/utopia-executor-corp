import discord
from discord.ext import commands

import cogs.data.database as database
import cogs.data.checks as checks

class Dev:
    def __init__(self, bot):
        self.bot = bot


    @commands.group()
    async def dev(self, ctx):
        pass

    @dev.command()
    @commands.check(checks.dev_check)
    async def points(self, ctx, user: discord.User = None):
        if user is None:
            user = ctx.message.author
        old_points = await database.get_points(user.id)
        await ctx.send("Current amount for {} is `{}`. What should the new points value be? Note that if you remove points then the position wont update.".format(user.name, old_points))

        def check(message):
            return message.author.id == ctx.message.author.id and message.channel.id == ctx.message.channel.id

        amount = int((await self.bot.wait_for("message", check=check)).content)

        if old_points > amount:
            change = old_points - (old_points - amount)
            await database.remove_points(user.id, change)
        elif old_points < amount:
            change = amount - old_points
            await database.add_points(user.id, change)

    @dev.command()
    @commands.check(checks.dev_check)
    async def position(self, ctx, member: discord.Member = None):
        if member is None:
            member = ctx.message.author
        old_pos = await database.get_position(member.id)
        await ctx.send("Current position for {} is `{}`. What should the new position be? Send the ID.".format(member.name, old_pos))

        def check(message):
            return message.author.id == member.id and message.channel.id == ctx.message.channel.id

        new_pos = int((await self.bot.wait_for("message", check=check)).content)

        positon = []
        for pos in database.postions:
            positon.append(pos['name'])
        for role in member.roles:
            if role.name in positon:
                await member.remove_roles(role)

        await database.set_position(member.id, new_pos)
        new_role = discord.utils.get(ctx.message.guild.roles, name=database.positions[new_pos]['name'])
        await member.add_roles(new_role)
        await database.set_points(member.id, database.positions[new_pos]['base'])


def setup(bot):
    bot.add_cog(Dev(bot))

def teardown(bot):
    bot.remove_cog('Dev')

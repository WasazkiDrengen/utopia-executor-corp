import discord
from discord.ext import commands

import cogs.data.database as database

import random

def commands_check(ctx):
    if "Staff" in [role.name for role in ctx.message.author.roles]: return True
    return ctx.channel.id == config.commands_channel

class Points:
    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    @commands.check(commands_check)
    async def points(self, ctx, user: discord.User = None):
        if user is None:
            user = ctx.message.author
        await ctx.send(await database.get_points(user.id))

    @commands.command()
    @commands.check(commands_check)
    async def pay(self, ctx, user: discord.User = None, amount: int = None, *, reason: str = None):
        if user is None or amount is None or reason is None:
            return await ctx.send("Please add a user, amount and a reason for the transaction.")

        if amount >= 900 or amount <= 0 or await database.get_points(ctx.message.author.id) <= amount:
            return await ctx.send("Invalid amount.")

        sender = await database.remove_points(ctx.message.author.id, amount)
        getter = await database.add_points(user.id, amount)
        await ctx.send("You now have `{}` and {} now have `{}`".format(sender, user.display_name, getter))

    async def on_message(self, message):
        position = await database.get_position(message.author.id)
        points = await database.get_points(message.author.id)
        if points >= database.positions[database.positions[position]['next']]['base']:
            old_role = discord.utils.get(message.guild.roles, name=database.positions[position]['name'])
            new_pos = await database.update_position(message.author.id)
            new_role = discord.utils.get(message.guild.roles, name=database.positions[new_pos]['name'])
            await message.author.add_roles(role, reason="Level up!")
            await message.author.remove_roles(old_role, reason="Level up!")
            await message.author.send("Congrats You are now a {} :heart:.".format(database.positions[new_pos]['name']))

        if random.choice([1, 2, 3, 4, 5]) == 1:
            await database.add_points(message.author.id, 1)



def setup(bot):
    bot.add_cog(Points(bot))

def teardown(bot):
    bot.remove_cog('Points')

import discord
from discord.ext import commands

import re

import cogs.data.checks as checks

animals = {"NAME": "Animals", "birb": "*birb sees camera* **click**", "shibe": "Say hello to my dog, a shibe", "dog": "*woof woof* STOP BARKING!!!", "cat": "*glass breaks* Really?!!!", "redpanda": "Doesnt look like a red panda, im just saying", "panda": "Wanna see a panda? I have just the right thing for you"}
fun = {"NAME": "Fun", "8ball [question]": "I have the answers of the universe...", "game": "Here's how to get a free game", "urban [word]": "What is a carrot?", "gamenight": "What are we gonna play this week?"}
#fun includes gamenight
info = {"NAME": "Info", "member [member]": "What roles does this person have, what is its ID?", "user [user]": "What is this persons discriminator?", "emoji [emoji]": "When was this emoji created", "role [role]": "Who has this role?",
        "channel [channel]": "Is this channel NSFW?", "server": "Is this server officially declared large?"}
mod = {"NAME": "Mod", "deny": "You changed your mind? You dont agree to the rules?", "delete [messages] [reason]": "I dont like that message, get it away from me!",
        "clear [member/channel] [reason]": "Remove all messages from this user in this channel or all messages in the channel.", "ticket (reason)": "I would like to report a bug or an issue", "close": "Problem fixed!",
        "warn [member] [reason]": "He didn't behave so well", "mute [member] [time] [reason]": "Shut your mouth!", "unmute [member] [reason]": "You can talk again buddy..", "kick [member] [reason]": "Leave the party, you're not welcome here!",
        "removeuser [member] [reason]": "He doesnt have right to gain points", "ban [member] [reason]": "You didn't listen to me, now GET OUT!!"}
#mod includes join

cmds = [animals, fun, info, mod]

class Help:
    def __init__(self, bot):
        self.bot = bot



    def command_search(self, search):
        comds = [comd.split(" ")[0] for comd in cmds]
        regex = re.findall(str(comds), search)
        if len(regex) > 0:
            return True


    @commands.command()
    @commands.check(checks.utopia_check)
    async def help(self, ctx, search: str = None):
        if search is None:
            for cog in cmds:
                embed = discord.Embed(title=cog["NAME"], colour=discord.Colour.dark_teal())
                for comand in cog.keys():
                    if comand == "NAME":
                        continue
                    embed.add_field(name=comand, value=cog[comand], inline=False)
                embed.set_footer(text="[] means needed, () means optional")
                await ctx.send(embed=embed)
        else:
            for cog in cmds:
                embed = discord.Embed(title=cog["NAME"], colour=discord.Colour.dark_teal())
                for comand in cog.keys():
                    if comand == "NAME":
                        continue
                    elif self.command_search(search) is True:
                        embed.add_field(name=comand, value=cg[comand], inline=False)
                if len(embed.fields) <= 0:
                    embed.set_footer(text="Sorry we couldn't find any commands with {}".format(search))
                else:
                    embed.set_footer(text="[] means needed, () means optional")
                await ctx.send(embed=embed)



def setup(bot):
    bot.add_cog(Help(bot))

def teardown(bot):
    bot.remove_cog('Help')

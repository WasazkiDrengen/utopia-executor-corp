import discord
from discord.ext import commands

import random

import cogs.data.database as database
import cogs.data.checks as checks
import cogs.data.config as config


class Join:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.check(checks.server_check)
    async def accept(self, ctx):
        peasant = discord.utils.get(ctx.message.guild.roles, name="Peasants")
        await ctx.message.author.add_roles(peasant, reason="Accepting Rules")

        await database.create_user(ctx.message.author.id)
        await ctx.message.author.send("Thank you for accepting `The Rules of Utopia`❤.")
        embed = discord.Embed(title=ctx.message.author.display_name, description="Welcome {}, hope you will stay! :heart:".format(ctx.message.author.mention), colour=random.choice([discord.Colour.blue(), discord.Colour.dark_blue()]))
        await self.bot.get_channel(config.welcome_channel).send(embed=embed)

    @commands.command()
    @commands.check(checks.server_check)
    async def deny(self, ctx):
        embed = discord.Embed(title="Kick", description="`{}` denied the rules.".format(member.display_name),
                                color=discord.Colour.red())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Highest Role", value=member.top_role)
        embed.add_field(name="Joined At", value=member.joined_at)
        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)
        await ctx.guild.kick(user=member, reason="Denied our rules.")
        await ctx.send(embed=embed)


    async def on_message(self, message):
        if message.channel.id == config.gate_channel and message.author.bot is False and (await database.get_staff(message.guild.id)) not in [role.name for role in message.author.roles]:
            if message.content.upper().startswith("U!ACCEPT"):
                pass
            else:
                await message.delete()

def setup(bot):
    bot.add_cog(Join(bot))

def teardown(bot):
    bot.remove_cog('Join')

import discord
from discord.ext import commands

from typing import Union

import cogs.data.checks as checks
import cogs.data.database as database


class Info:
    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    @commands.check(checks.staff_check)
    async def member(self, ctx, member: discord.Member = None):
        if member is None:
            member = ctx.message.author
        elif isinstance(member, discord.Member) is False:
            return await ctx.send("Please add a member, either a mention, ID or full name.")

        if member.status is discord.Status.online:
            embed = discord.Embed(title="Member", description="Info about `{}`".format(member.display_name), color=discord.Colour.green())
        elif member.status is discord.Status.idle:
            embed = discord.Embed(title="Member", description="Info about `{}`".format(member.display_name), color=discord.Colour.orange())
        elif member.status is discord.Status.do_not_disturb:
            embed = discord.Embed(title="Member", description="Info about `{}`".format(member.display_name), color=discord.Colour.red())
        else:
            embed = discord.Embed(title="Member", description="Info about `{}`".format(member.display_name))

        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Name", value=member.name)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Discriminator", value=member.discriminator)
        if member.nick is None:
            embed.add_field(name="Nickname", value="None")
        else:
            embed.add_field(name="Nickname", value=member.nick)
        if member.activity is None:
            embed.add_field(name="Game", value="None")
        else:
            embed.add_field(name="Game", value=member.activity.name)
        embed.add_field(name="Points", value=await database.get_points(member.id))
        embed.add_field(name="Top Role", value=member.top_role.mention)
        embed.add_field(name="Bot", value=member.bot)
        embed.add_field(name="Joined At", value=member.joined_at)
        embed.add_field(name="Created At", value=member.created_at)

        member_roles = ''
        for role in member.roles:
            member_roles += str(role.mention + " ")
        if member_roles == '':
            member_roles = "None"
        embed.add_field(name="Roles", value=member_roles, inline=False)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.staff_check)
    async def user(self, ctx, user: discord.User = None):
        if user is None:
            user = ctx.message.author
        elif isinstance(user, discord.User) is False:
            return await ctx.send("Please add a user, either a mention, ID or full name.")

        embed = discord.Embed(title="User", description="Info about `{}`".format(user.display_name))
        embed.set_thumbnail(url=user.avatar_url)
        embed.add_field(name="Name", value=user.name)
        embed.add_field(name="ID", value=user.id)
        embed.add_field(name="Discriminator", value=user.discriminator)
        embed.add_field(name="Created At", value=user.created_at)
        embed.add_field(name="Points", value=await database.get_points(user.id))
        embed.add_field(name="Bot", value=user.bot)
        embed.add_field(name="Mention", value=user.mention)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def emoji(self, ctx, emoji: discord.Emoji = None):
        if emoji is None:
            return await ctx.send("Please send the id, name or the emoji itself.")

        embed = discord.Embed(title="Emoji", description="Info about `{}`".format(emoji.name))

        embed.set_thumbnail(url=emoji.url)
        embed.add_field(name="Name", value=emoji.name)
        embed.add_field(name="ID", value=emoji.id)
        embed.add_field(name="Server", value=emoji.guild.name)
        embed.add_field(name="Created At", value=emoji.created_at)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def role(self, ctx, *, role: discord.Role = None):
        if role is None:
            return await ctx.send("Please mention the role or send its name.")
        embed = discord.Embed(title="Role", description="Info about `{}`".format(role.name), colour=role.colour)
        embed.add_field(name="Name", value=role.name)
        embed.add_field(name="ID", value=role.id)
        embed.add_field(name="Server", value=role.guild.name)
        embed.add_field(name="Created At", value=role.created_at)
        embed.add_field(name="Hoist", value=role.hoist)
        embed.add_field(name="Position", value=role.position)

        members = ''
        for member in role.members:
            members += str(member.mention + " ")
        if members is '':
            members = "None"
        embed.add_field(name="Members", value=members)
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.staff_check)
    async def channel(self, ctx, channel: Union[discord.TextChannel, discord.VoiceChannel, discord.CategoryChannel] = None):
        if channel is None:
            channel = ctx.channel

        if isinstance(channel, discord.TextChannel):
            embed = discord.Embed(title="Text Channel", description="Info about `{}`".format(channel.name))
            embed.set_thumbnail(url=channel.guild.icon_url)
            embed.add_field(name="Name", value=channel.name)
            embed.add_field(name="ID", value=channel.id)
            embed.add_field(name="Server", value=channel.guild.name)
            embed.add_field(name="Created At", value=channel.created_at)
            if channel.topic is None:
                embed.add_field(name="Topic", value="None")
            else:
                embed.add_field(name="Topic", value=channel.topic)

            embed.add_field(name="Position", value=channel.position)
            if channel.category is None:
                embed.add_field(name="Category", value="None")
            else:
                embed.add_field(name="Category", value=channel.category.name)
            embed.add_field(name="NSFW", value=channel.is_nsfw())

            pins = ''
            for pin in await channel.pins():
                pins += str(pin.content + " ")
            if pins == '':
                pins = "None"
            embed.add_field(name="Pins", value=pins)
            await ctx.send(embed=embed)

        elif isinstance(channel, discord.VoiceChannel):
            embed = discord.Embed(title="Voice Channel", description="Info about `{}`".format(channel.name))
            embed.set_thumbnail(url=channel.guild.icon_url)
            embed.add_field(name="Name", value=channel.name)
            embed.add_field(name="ID", value=channel.id)
            embed.add_field(name="Server", value=channel.guild)
            embed.add_field(name="Created At", value=channel.created_at)
            embed.add_field(name="BitRate", value=channel.bitrate)
            embed.add_field(name="User Limit", value=channel.user_limit)
            if channel.category is None:
                embed.add_field(name="Category", value="None")
            else:
                embed.add_field(name="Category", value=channel.category.name)
            embed.add_field(name="Position", value=channel.position)
            await ctx.send(embed=embed)

        elif isinstance(channel, discord.CategoryChannel):
            embed = discord.Embed(title="Category", description="Info about `{}`".format(channel.name))
            embed.set_thumbnail(url=channel.guild.icon_url)
            embed.add_field(name="Name", value=channel.name)
            embed.add_field(name="ID", value=channel.id)
            embed.add_field(name="Server", value=channel.guild)
            embed.add_field(name="Created At", value=channel.created_at)
            embed.add_field(name="Position", value=channel.position)
            embed.add_field(name="NSFW", value=channel.is_nsfw())

            channels = ''
            for chnl in channel.channels:
                channels += str(chnl.mention + " ")
            if channels == '':
                channels = "None"
            embed.add_field(name="Channels", value=channels, inline=False)
            await ctx.send(embed=embed)
        else:
            await ctx.send("That is not a channel, category or voice channel.")

    @commands.command()
    @commands.check(checks.staff_check)
    async def server(self, ctx):
        server = ctx.message.guild
        embed = discord.Embed(title="Server", description="Info about `{}`".format(server.name))
        if server.icon_url is None:
            pass
        else:
            embed.set_thumbnail(url=server.icon_url)
        embed.add_field(name="Name", value=server.name)
        embed.add_field(name="ID", value=server.id)
        embed.add_field(name="Owner", value=server.owner.mention)
        embed.add_field(name="Created At", value=server.created_at)
        embed.add_field(name="Large", value=server.large)
        embed.add_field(name="Member Count", value=server.member_count)

        roles = ''
        for role in server.roles:
            roles += str(role.mention + " ")
        if roles == '':
            roles = "None"
        embed.add_field(name="Roles", value=roles, inline=False)

        channels = ''
        for channel in server.text_channels:
            channels += str(channel.mention + " ")
        if channels == '':
            channels = None
        embed.add_field(name="Text Channels", value=channels, inline=False)

        await ctx.send(embed=embed)

    @commands.group()
    async def settings(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send('That is not a setting')
            await ctx.send("""Settings commands:
settings prefix [prefix] Change server prefix. `u!` and `U!` will always work.
settings staff [role] Change the role that is staff, the members with this role can use the staff commands.
settings automod [automod] Turn on/off automod AKA Sentiel AKA Anti-Invite""")

    @settings.command()
    @commands.check(checks.server_owner)
    async def prefix(self, ctx, prefix: str = None):
        if prefix is None:
            prefix = "!"
        new_prefix = await database.change_prefix(ctx.message.guild.id, prefix)
        await ctx.send("The prefix is now `{}`".format(new_prefix))

    @settings.command()
    @commands.check(checks.server_owner)
    async def staff(self, ctx, role: discord.Role = None):
        if role is None:
            await ctx.send("A role please?")
        new_staff = await database.change_staff(ctx.message.guild.id, role.name)
        await ctx.send("The staff role is now `{}`".format(role.name))

    @settings.command()
    @commands.check(checks.server_owner)
    async def automod(self, ctx, automod: bool = None):
        if automod is None:
            automod = False
        new_automod = await database.change_automod(ctx.message.guild.id, automod)
        if new_automod is True: new_automod = "On"
        elif new_automod is False: new_automod = "Off"
        await ctx.send("Automod AKA Sentiel AKA Anti-Invite is now `{}`".format(new_automod))


def setup(bot):
    bot.add_cog(Info(bot))

def teardown(bot):
    bot.remove_cog('Info')

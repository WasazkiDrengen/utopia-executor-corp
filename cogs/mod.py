import discord
from discord.ext import commands

import datetime
import asyncio
from typing import Union
import re

import cogs.data.checks as checks
import cogs.data.database as database


class Mod:
    def __init__(self, bot):
        self.bot = bot

    async def verify(self, ctx, task):
        await ctx.send("Are you sure you want to {}?".format(task))

        def author_check(message):
            return message.author.id == ctx.message.author.id

        message = await self.bot.wait_for('message', check=author_check)

        if message.content.upper()[0] == "Y":
            return True

        elif message.content.upper()[0] == "N":
            return False
        else:
            return None

    @commands.command()
    @commands.check(checks.staff_check)
    @commands.check(checks.server_check)
    async def rules(self, ctx):
        await ctx.message.delete()
        embed = discord.Embed(title="Low severity", colour=discord.Colour.green())
        embed.add_field(name="Do not spam", value="Don't send many messages with the same content or sending many random emojis. Just dont flood the channels.", inline=False)
        embed.add_field(name="Do not be rude to another member", value="Don't say something that you know will mentally hurt someone.", inline=False)
        embed.add_field(name="Do not troll", value="Don't try to confuse or trick anyone into doing anything, this includes impersonating a member.", inline=False)
        embed.add_field(name="Try to use channels for the intended purpose", value="Please try to only use the channels for their purpose, only commands in the commands channel, only media in the media channel", inline=False)
        embed.add_field(name="Do not ghost ping", value="Any kind of ghost pinging is not allowed, meaning that you ping and remove the message.", inline=False)
        embed.add_field(name="Do not make a nickname that is not mentionable", value="No invisible nicknames, this includes characters that wont show or characters that is not using a ASCII lettering.", inline=False)
        embed.set_footer(text="A verbal warning or a mute.")
        await ctx.send(embed=embed)

        embed = discord.Embed(title="Medium severity", colour=discord.Colour.orange())
        embed.add_field(name="Do not advertise", value="Don't send invite links to other servers or advertise a website/service. This includes DM advertising without consent.", inline=False)
        embed.add_field(name="Do not threathen", value="Don't go around threathening members for stuff like hacking, doxing or showing personal information.", inline=False)
        embed.add_field(name="Do not argue with staff", value="Don't go argue with the decisions of staff. What the staff says is the final word.", inline=False)
        embed.set_footer(text="A warning, longer mute or a kick.")
        await ctx.send(embed=embed)

        embed = discord.Embed(title="High severity", colour=discord.Colour.red())
        embed.add_field(name="Do not send malicious software", value="Don't send links to malicious software or scams. This includes hacks or cheats for different games.", inline=False)
        embed.add_field(name="Do not be inappropriate", value="Don't send anything NSFW. No Hentai, Porn or Nudity.", inline=False)
        embed.add_field(name="Do not excessively brake the rules.", value="Don't go around and brake the rules too many times. Breaking a rule does have big consequences.", inline=False)
        embed.add_field(name="Follow the ToS", value="If it wasn't clear enough, everyone needs to follow discord's ToS. If we find any selfbots or anything else that breaks the ToS we will ban and report it.", inline=False)
        embed.set_footer(text="A ban, serious warn or very long mute.")
        await ctx.send(embed=embed)

        embed = discord.Embed(description="Accepting these rules will ~~__*give your life to Utopia Corps*__~~ allow you to enter Utopia. Just `u!accept`, but the legend says that if you `u!deny` then according to the 43th [Great Guru's decree](https://docs.google.com/document/d/1SXspWTMGzjIJr2gplSmLkR6HLeZIWu2x1EZz2hnT7xY/edit?usp=sharing) you shall get kicked out of Utopia City...", colour=discord.Colour.dark_red())
        await ctx.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def delete(self, ctx, messages: int = 1, *, reason: str = None):
        if reason is None:
            return await ctx.send("Please add a reason.")
        if await self.verify(ctx, 'delete messages') is False:
            return await ctx.send("Ok, stopping delete")
        mgs = []
        async for message in ctx.channel.history(limit=messages + 1):
            mgs.append(message)
        await ctx.channel.delete_messages(mgs)


        embed = discord.Embed(title="Messages Delete", description="`{}` messages was deleted.".format(messages),
                                color=discord.Colour.orange())


        embed.add_field(name="Deleter", value=ctx.message.author.display_name)
        embed.add_field(name="Reason", value=reason)
        embed.add_field(name="Messages", value=messages)

        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

        await ctx.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def clear(self, ctx, argument: Union[discord.Member, discord.TextChannel], *, reason: str = None):
        if argument is None or reason is None:
            return await ctx.send("Please specify a mention like a channel or user and a reason.")
        if await self.verify(ctx, 'Clear all messages') is False:
            return await ctx.send("Ok, stopping clear")

        if isinstance(argument, discord.Member):
            member = argument
            mgs = []

            async for message in ctx.channel.history(limit=None):
                if len(mgs) == 100:
                    break
                if message.author is member:
                    mgs.append(message)
            await ctx.channel.delete_messages(mgs)

            embed = discord.Embed(title="Messages Delete", description="`{}` messages from `{}` was deleted.".format(len(mgs), member.display_name),
                                    color=discord.Colour.orange())
            embed.add_field(name="Deleter", value=ctx.message.author.display_name)
            embed.add_field(name="Reason", value=reason)
            embed.add_field(name="Messages Author", value=member.display_name)
            embed.add_field(name="Messages", value=len(mgs))
            embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)
            await ctx.send(embed=embed)

        elif isinstance(argument, discord.TextChannel):
            channel = argument
            mgs = []

            async for message in channel.history(limit=None):
                if len(mgs) == 100:
                    break
                mgs.append(message)

            await ctx.channel.delete_messages(mgs)

            embed = discord.Embed(title="Messages Delete", description="`{}` messages from `{}` was deleted.".format(len(mgs), channel.name),
                                    color=discord.Colour.orange())
            embed.add_field(name="Deleter", value=ctx.message.author.display_name)
            embed.add_field(name="Reason", value=reason)
            embed.add_field(name="Channel", value=channel.name)
            embed.add_field(name="Messages", value=len(mgs))
            embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)
            await ctx.send(embed=embed)
        else:
            await ctx.send("That was not a member or channel.")


    @commands.command()
    @commands.check(checks.utopia_check)
    async def ticket(self, ctx, *, reason: str = "I need help"):
        staff = discord.utils.get(ctx.message.guild.roles, name=await database.get_staff(ctx.message.guild.id))
        permissions = {
            ctx.message.guild.default_role: discord.PermissionOverwrite(read_messages=False),
            ctx.message.guild.me: discord.PermissionOverwrite(read_messages=True),
            ctx.message.author: discord.PermissionOverwrite(read_messages=True),
            staff: discord.PermissionOverwrite(read_messages=True)
        }
        ticket = "ticket-" + str(await database.create_ticket(ctx.message.guild.id))
        category = discord.utils.get(ctx.message.guild.categories, name="Tickets")
        if category is None:
            category = await ctx.message.guild.create_category_channel(name="Tickets")
        channel = await ctx.message.guild.create_text_channel(name=ticket, overwrites=permissions, category=category)
        embed = discord.Embed(title="Hey {}!".format(ctx.message.author.display_name), description="Thanks for reaching out to us, we will get back to you ASAP \n \n You said: {}".format(reason), colour=discord.Colour.green())
        await channel.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def close(self, ctx):
        if ctx.channel.name.startswith("ticket-"):
            await ctx.channel.delete(reason="Ticket Closed.")


    @commands.command()
    @commands.check(checks.staff_check)
    async def warn(self, ctx, member: discord.Member = None, *, reason: str = None):
        if member is None or reason is None:
            return await ctx.send("Please add a user and a reason for the warn.")
        if await self.verify(ctx, 'warn a user') is False:
            return await ctx.send("Ok, stopping warn")

        embed = discord.Embed(title="Warning", description="`{}` was warned by `{}`.".format(member.display_name, ctx.message.author.display_name),
                                color=discord.Colour.orange())
        embed.set_thumbnail(url=member.avatar_url)

        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Highest Role", value=member.top_role)
        embed.add_field(name="Joined At", value=member.joined_at)

        embed.add_field(name="Warner", value=ctx.message.author.display_name)
        embed.add_field(name="Reason", value=reason)

        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.staff_check)
    async def mute(self, ctx, member: discord.Member = None, time: int = None, *, reason: str = None):
        if member is None or time is None or reason is None:
            return await ctx.send("Please add a user, time and a reason for the mute..")
        if await self.verify(ctx, 'mute a user') is False:
            return await ctx.send("Ok, stopping mute")

        mute = discord.utils.get(ctx.message.guild.roles, name="Voiceless")

        embed = discord.Embed(title="Mute", description="`{}` was muted by `{}` for `{}` minutes.".format(member.display_name, ctx.message.author.display_name, str(time)),
                                color=discord.Colour.red())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Highest Role", value=member.top_role)
        embed.add_field(name="Joined At", value=member.joined_at)
        embed.add_field(name="Muter", value=ctx.message.author.display_name)
        embed.add_field(name="Reason", value=reason)
        embed.add_field(name="Mute Length", value=str(time))
        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

        await member.add_roles(mute)
        await ctx.send(embed=embed)
        await asyncio.sleep(time * 60)

        embed = discord.Embed(title="UnMute", description="`{}` is now unmuted.".format(member.display_name),
                                color=discord.Colour.orange())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Mute Length", value=str(time))
        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)
        try:
            await member.remove_roles(mute)
        except Exception:
            return
        await ctx.send(embed=embed)


    @commands.command()
    @commands.check(checks.staff_check)
    async def unmute(self, ctx, member: discord.Member = None, *, reason: str = None):
        if member is None or reason is None:
            return await ctx.send("Please add a user and a reason for the mute..")
        if await self.verify(ctx, 'unmute a user') is False:
            return await ctx.send("Ok, stopping unmute")

        mute = discord.utils.get(ctx.message.guild.roles, name="Voiceless")

        embed = discord.Embed(title="UnMute", description="`{}` was unmuted by `{}`".format(member.display_name, ctx.message.author.display_name),
                                    color=discord.Colour.orange())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Unmuter", value=ctx.message.author.display_name)
        embed.add_field(name="Reason", value=reason)
        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

        await member.remove_roles(mute)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def kick(self, ctx, member: discord.Member = None, *, reason: str = None):
        if member is None or reason is None:
            return await ctx.send("Please add a user and a reason for the kick..")
        if await self.verify(ctx, 'kick a member') is False:
            return await ctx.send("Ok, stopping kick")

        embed = discord.Embed(title="Kick", description="`{}` was kicked by `{}` for `{}`.".format(member.display_name, ctx.message.author.display_name, reason),
                                color=discord.Colour.red())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Highest Role", value=member.top_role)
        embed.add_field(name="Joined At", value=member.joined_at)
        embed.add_field(name="Kicker", value=ctx.message.author.display_name)
        embed.add_field(name="Reason", value=reason)
        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

        try:
            await member.send(embed=embed)
        except:
            pass
        await ctx.guild.kick(user=member, reason=reason)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.check(checks.staff_check)
    async def ban(self, ctx, member: discord.Member = None, *, reason: str = None):
        if member is None or reason is None:
            return await ctx.send("Please add a user and a reason for the ban..")
        if await self.verify(ctx, 'ban a user') is False:
            return await ctx.send("Ok, stopping ban")

        embed = discord.Embed(title="Ban!!", description="`{}` was banned by `{}` for `{}`.".format(member.display_name, ctx.message.author.display_name, reason),
                                color=discord.Colour.red())
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="Member", value=member.name + "#" + member.discriminator)
        embed.add_field(name="ID", value=member.id)
        embed.add_field(name="Highest Role", value=member.top_role)
        embed.add_field(name="Joined At", value=member.joined_at)
        embed.add_field(name="Banner", value=ctx.message.author.display_name)
        embed.add_field(name="Reason", value=reason)
        embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

        try:
            await member.send(embed=embed)
        except:
            pass
        await ctx.guild.ban(user=member, reason=reason, delete_message_days=7)
        await ctx.send(embed=embed)

    #automod/Sentiel

    async def on_member_update(self, before, after):
        if before.name != after.name:
            regex = re.findall(r'discord\.gg\/(\w+)', after.name)
            if len(regex) != 0:
                embed = discord.Embed(title="Kick", description="`{}` was kicked by `{}` for `{}`.".format(after.display_name, "Sentiel", "having a invite-link name"),
                                color=discord.Colour.red())
                embed.set_thumbnail(url=after.avatar_url)
                embed.add_field(name="Member", value=after.name + "#" + after.discriminator)
                embed.add_field(name="ID", value=after.id)
                embed.add_field(name="Highest Role", value=after.top_role)
                embed.add_field(name="Joined At", value=after.joined_at)
                embed.add_field(name="Kicker", value="Sentiel")
                embed.add_field(name="Reason", value="Because it had a invite-link name")
                embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

                try:
                    await after.send(embed=embed)
                except:
                    pass
                await after.guild.send(embed=embed)
                await after.guild.kick(user=after, reason="Having a invite-link name")

        elif before.nick != after.nick and after.nick is not None:
            regex = re.findall(r'discord\.gg\/(\w+)', after.nick)
            if len(regex) != 0:
                embed = discord.Embed(title="Kick", description="`{}` was kicked by `{}` for `{}`.".format(after.display_name, "Sentiel", "having a invite-link nickname"),
                                color=discord.Colour.red())
                embed.set_thumbnail(url=after.avatar_url)
                embed.add_field(name="Member", value=after.name + "#" + after.discriminator)
                embed.add_field(name="ID", value=after.id)
                embed.add_field(name="Highest Role", value=after.top_role)
                embed.add_field(name="Joined At", value=after.joined_at)
                embed.add_field(name="Kicker", value="Sentiel")
                embed.add_field(name="Reason", value="Because it had a invite-link nickname")
                embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

                try:
                    await after.send(embed=embed)
                except:
                    pass
                await after.guild.send(embed=embed)
                await after.guild.kick(user=after, reason="Having a invite-link nickname")


        elif before.activity != after.activity and after.activity is not None:
            regex = re.findall(r'discord\.gg\/(\w+)', after.activity.name)
            if len(regex) != 0:
                embed = discord.Embed(title="Kick", description="`{}` was kicked by `{}` for `{}`.".format(member.display_name, "Sentiel", "having a invite-link activity"),
                                color=discord.Colour.red())
                embed.set_thumbnail(url=after.avatar_url)
                embed.add_field(name="Member", value=after.name + "#" + after.discriminator)
                embed.add_field(name="ID", value=after.id)
                embed.add_field(name="Highest Role", value=after.top_role)
                embed.add_field(name="Joined At", value=after.joined_at)
                embed.add_field(name="Kicker", value="Sentiel")
                embed.add_field(name="Reason", value="Because it had a invite-link activity")
                embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

                try:
                    await after.send(embed=embed)
                except:
                    pass
                await after.guild.send(embed=embed)
                await after.guild.kick(user=after, reason="Having a invite-link activity")

    async def on_message(self, message):
        regex = re.findall(r'discord\.gg\/(\w+)', message.content)
        if len(regex) != 0:
            embed = discord.Embed(title="Kick", description="`{}` was kicked by `{}` for `{}`.".format(message.author.display_name, "Sentiel", "sending a invite-link"),
                            color=discord.Colour.red())
            embed.set_thumbnail(url=message.author.avatar_url)
            embed.add_field(name="Member", value=message.author.name + "#" + message.author.discriminator)
            embed.add_field(name="ID", value=message.author.id)
            embed.add_field(name="Highest Role", value=message.author.top_role)
            embed.add_field(name="Joined At", value=message.author.joined_at)
            embed.add_field(name="Kicker", value="Sentiel")
            embed.add_field(name="Reason", value="Because it sent a invite link")
            embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

            try:
                await message.author.send(embed=embed)
            except:
                pass
            await after.guild.send(embed=embed)
            await message.guild.kick(user=message.author, reason="Sending a invite-link")

    async def on_message_edit(self, before, after):
        regex = re.findall(r'discord\.gg\/(\w+)', after.content)
        if len(regex) != 0:
            embed = discord.Embed(title="Kick", description="`{}` was kicked by `{}` for `{}`.".format(after.author.display_name, "Sentiel", "sending a invite-link"),
                            color=discord.Colour.red())
            embed.set_thumbnail(url=after.author.avatar_url)
            embed.add_field(name="Member", value=after.author.name + "#" + after.author.discriminator)
            embed.add_field(name="ID", value=after.author.id)
            embed.add_field(name="Highest Role", value=after.author.top_role)
            embed.add_field(name="Joined At", value=after.author.joined_at)
            embed.add_field(name="Kicker", value="Sentiel")
            embed.add_field(name="Reason", value="Because it sent a invite link")
            embed.add_field(name="Time", value=datetime.datetime.utcnow(), inline=False)

            try:
                await after.send(embed=embed)
            except:
                pass
            await after.guild.send(embed=embed)
            await after.guild.kick(user=after, reason="Sending a invite-link")


async def punsishment_loop(bot):
    await asyncio.sleep(30)
    cases = await database.get_change_cases()
    for case in cases:
        guild = bot.get_guild(case['serverid'])
        member = bot.get_member(case['userid'])
        user = bot.get_user(case['userid'])
        if case['type'] == "WARN":
            await database.change_active(case['casenumber'], False)

        elif case['type'] == "MUTE":
            role = discord.utils.get(guild.roles, name="Voiceless")
            await database.change_active(case['casenumber'], False)
            await member.remove_roles(role)
            await member.send("You are now unmuted :heart:")

        elif case['type'] == "KICK":
            await database.change_active(case['casenumber'], False)

        elif case['type'] == "BAN"
            await database.change_active(case['casenumber'], False)
            awaitr guild.unban(user, case['reason'])

def setup(bot):
    asyncio.ensure_future(punsishment_loop(bot))
    bot.add_cog(Mod(bot))

def teardown(bot):
    bot.remove_cog('Mod')

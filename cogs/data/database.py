import discord
from discord.ext import commands

import asyncpg
import asyncio

positions = [
    {'name': "Peasant", 'base': 10, 'next': 1},
    {'name': "Lower Citizen", 'base': 50, 'next': 2},
    {'name': "Middle Citizen", 'base': 100, 'next': 3},
    {'name': "Higher Citizen", 'base': 200, 'next': 4},
    {'name': "Lower Utopist", 'base': 500, 'next': 5},
    {'name': "Middle Utopist", 'base': 1000, 'next': 6},
    {'name': "Higher Utopist", 'base': 5000, 'next': 7},
    {'name': "Great Utopist", 'base': 10000, 'next': {'name': "God", 'base': 100000000000000}}
        ]


async def connect():
    global pool
    pool = await asyncpg.create_pool(dsn='postgresql://postgres@localhost/utopia', host='127.0.0.1', user='bot', password='b0t')
    async with pool.acquire() as conn:
        await conn.execute("""CREATE TABLE IF NOT EXISTS Utopians(
                                UserID TEXT     NOT NULL,
                                Points INTEGER,
                                Position INTEGER,
                                PRIMARY KEY (UserID))""")
        await conn.execute("""CREATE TABLE IF NOT EXISTS Gamenights(
                                Week INTEGER    NOT NULL,
                                Name TEXT,
                                Description TEXT,
                                EmbedColour TEXT,
                                Systems TEXT,
                                Players INTEGER,
                                ImageURL TEXT,
                                DownloadURL TEXT,
                                Publisher TEXT,
                                PRIMARY KEY (Week))""")
        await conn.execute("""CREATE TABLE IF NOT EXISTS Servers(
                                ServerID TEXT    NOT NULL,
                                Prefix TEXT,
                                Ticket INTEGER,
                                AutoMod BOOL,
                                StaffName TEXT,
                                PRIMARY KEY (ServerID))""")
        await conn.execute("""CREATE TABLE IF NOT EXISTS Punishments(
                                UserID TEXT,
                                ServerID TEXT,
                                CaseNumber INTEGER,
                                Type TEXT,
                                Active BOOL,
                                Reason TEXT,
                                StartTime TIMESTAMP,
                                Duration INTEGER,
                                PRIMARY KEY (CaseNumber))""")

#Create pool
asyncio.ensure_future(connect())


#functions for table Utopians
async def does_user_exist(user_id: str):
    async with pool.acquire() as conn:
        user = await conn.fetchval("SELECT UserID FROM Utopians WHERE UserID=$1", str(user_id))
        if user is None:
            return False
        else:
            return True


async def create_user(user_id: str):
    async with pool.acquire() as conn:
        user = await does_user_exist(user_id)
        if user is False:
            await conn.execute("INSERT INTO Utopians VALUES ($1, $2, $3)", str(user_id), 10, 0)
            return True
        else:
            return False

async def delete_user(user_id: str):
    async with pool.acquire() as conn:
        user = await does_user_exist(user_id)
        if user is True:
            await conn.execute("UPDATE Utopians SET UserID=$1 WHERE UserID$2", str(user_id))
            return True
        else:
            return False

async def get_points(user_id: str):
    async with pool.acquire() as conn:
        await create_user(user_id)
        user = await conn.fetchval("SELECT Points FROM Utopians WHERE UserID=$1", str(user_id))
        return int(user)

async def remove_points(user_id: str, amount: int):
    async with pool.acquire() as conn:
        points = int(await get_points(user_id)) - amount
        await conn.execute("UPDATE Utopians SET Points=$1 WHERE UserID=$2", points, str(user_id))
        return points

async def add_points(user_id: str, amount: int):
    async with pool.acquire() as conn:
        points = int(await get_points(user_id)) + amount
        await conn.execute("UPDATE Utopians SET Points=$1 WHERE UserID=$2", points, str(user_id))
        return points

async def set_points(user_id: str, amount: int):
    async with pool.acquire() as conn:
        await conn.execute("UPDATE Utopians SET Points=$1 WHERE UserID=$2", int(amount), str(user_id))
        return amount

async def get_position(user_id: str):
    async with pool.acquire() as conn:
        await create_user(user_id)
        position = await conn.fetchval("SELECT Position FROM Utopians WHERE UserID=$1", str(user_id))
        return int(position)

async def update_position(user_id: str):
    async with pool.acquire() as conn:
        pos_id = await get_position(user_id)
        if await get_points(user_id) >= positions[positions[pos_id]['next']]['base']:
            await conn.execute("UPDATE Utopians SET Position=$1 WHERE UserID=$2", int(pos_id + 1), str(user_id))
            return int(pos_id + 1)
        else:
            return pos_id

async def set_position(user_id: str, position: int):
    async with pool.acquire() as conn:
        await conn.execute("UPDATE Utopians SET Position=$1 WHERE UserID=$2", position, str(user_id))
        return int(pos_id + 1)


#functions for table Gamenights
async def get_game(name: str = "VIEW", week: int = 0):
    async with pool.acquire() as conn:
        if week == 0 and name != "VIEW":
            game = await conn.fetchrow("SELECT * FROM Gamenights WHERE Name=$1", name)
            return game
        elif name == "VIEW" and week != 0:
            game = await conn.fetchrow("SELECT * FROM Gamenights WHERE Week=$1", 38)
            return game
        else:
            game = await conn.fetchrow("SELECT * FROM Gamenights WHERE Name=$1 AND Week=$2", name, week)
            return game

async def create_game(week: int, name: str, description: str, colour: str, systems: str, players: int, image_url: str, download_url: str, publisher: str):
    async with pool.acquire() as conn:
        await conn.execute("INSERT INTO Gamenights VALUES($1, $2, $3, $4, $5, $6, $7, $8)", week, name, description, colour, systems, players, image_url, download_url, publisher)
        return True


#functions for table Servers
async def does_server_exist(server_id: str):
    async with pool.acquire() as conn:
        server = await conn.fetchval("SELECT ServerID FROM Servers WHERE ServerID=$1", str(server_id))
        if server is None:
            return False
        else:
            return True

async def create_server(server_id: str):
    async with pool.acquire() as conn:
        server = await does_server_exist(server_id)
        if server is False:
            await conn.execute("INSERT INTO Servers VALUES ($1, $2, $3, $4, $5)", str(server_id), "!", 0, False, "Staff")
            return True
        else:
            return True

async def delete_server(server_id: str):
    async with pool.acquire() as conn:
        server = await does_server_exist(server_id)
        if server is True:
            await conn.execute("UPDATE Servers SET ServerID=$1 WHERE ServerID=$1", "REMOVED")
            return True
        else:
            return False


async def get_prefix(server_id: str):
    async with pool.acquire() as conn:
        await create_server(server_id)
        prefix = await conn.fetchval("SELECT Prefix FROM Servers WHERE ServerID=$1", str(server_id))
        return prefix

async def change_prefix(server_id: str, prefix: str):
    async with pool.acquire() as conn:
        server = await create_server(server_id)
        await conn.execute("UPDATE Servers SET Prefix=$1 WHERE ServerID=$2", str(prefix), str(server_id))
        return prefix


async def create_ticket(server_id: str):
    async with pool.acquire() as conn:
        server = await create_server(server_id)
        ticket = await conn.fetchval("SELECT Ticket FROM Servers WHERE ServerID=$1", str(server_id))
        new_ticket = int(ticket) + 1
        await conn.execute("UPDATE Servers SET Ticket=$1 WHERE ServerID=$2", int(new_ticket), str(server_id))
        return ticket

async def get_staff(server_id: str):
    async with pool.acquire() as conn:
        server = await create_server(server_id)
        staff_name = await conn.fetchval("SELECT StaffName FROM Servers WHERE ServerID=$1", str(server_id))
        return staff_name

async def change_staff(server_id: str, staff_name: str):
    async with pool.acquire() as conn:
        server = await create_server(server_id)
        await conn.execute("UPDATE Servers SET StaffName=$1 WHERE ServerID=$2", str(staff_name), str(server_id))
        return staff_name

async def get_automod(server_id: str):
    async with pool.acquire() as conn:
        server = await create_server(server_id)
        automod = await conn.fetchval("SELECT AutoMod FROM Servers WHERE ServerID=$1", str(server_id))
        return bool(automod)

async def change_automod(server_id: str, automod: bool):
    async with pool.acquire() as conn:
        server = await create_server(server_id)
        await conn.execute("UPDATE Servers SET AutoMod=$1 WHERE ServerID=$2", bool(automod), str(server_id))
        return automod

#Punishments functions

async def get_case(user_id: str = 0, number: int = 0):
    async with pool.acquire() as conn:
        if number == 0:
            cases = await conn.fetch("SELECT * FROM Punishments WHERE UserID=$1", str(user_id))
            return cases
        elif user_id == 0:
            case = await conn.fetchrow("SELECT * FROM Punishments WHERE CaseNumber=$1", int(number))
            return case
        elif user_id != 0 and number != 0:
            case = await conn.fetchrow("SELECT * FROM Punishments WHERE UserID=$1 AND CaseNumber=$2", str(user_id), int(number))
            return case

async def create_case(user_id: str, server_id: str, type: str, reason: str, duration: int):
    async with pool.acquire() as conn:
        casenumber = (await conn.fetch("SELECT * FROM Punishments ORDER BY DESC"))[0]
        await conn.execute("INSERT INTO Punishments VALUES ($1, $2, $3, $4, $5, $6, $7, $8)", str(user_id), str(server_id), int(casenumber), str(type.upper()), True, "CURRENT_TIMESTAMP", int(duration))
        case = await conn.fetch("SELECT * FROM Punishments WHERE CaseNumber=$1", int(casenumber))
        return case

async def get_change_cases():
    async with pool.acquire() as conn:
        cases = await conn.fetch("SELECT * FROM Punishments WHERE Active=$1 AND (StartTime + (Duration * interval '1 minute')) < CURRENT_TIMESTAMP", True)
        return cases

async def change_active(number: int, active: bool):
    async with pool.acquire() as conn:
        await conn.execute("UPDATE Punishments SET Active=$1 WHERE CaseNumber=$2", bool(active), int(number))
        return active

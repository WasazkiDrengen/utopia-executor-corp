
import cogs.data.database as database
import cogs.data.config as config

async def staff_check(ctx):
    if (await database.get_staff(ctx.message.guild.id)) in [role.name for role in ctx.message.author.roles]: return True
    if ctx.message.author.id == ctx.message.guild.owner_id: return True

async def utopia_check(ctx):
    if ctx.message.guild.id == config.utopia and ctx.channel.id == config.commands_channel: return True
    if ctx.message.guild != config.utopia: return True
    if (await database.get_staff(ctx.message.guild.id)) in [role.name for role in ctx.message.author.roles]: return True
    if ctx.message.author.id == ctx.message.guild.owner_id: return True

def server_check(ctx):
    if ctx.message.guild == config.utopia: return True

async def special_utopia_check(ctx):
    if ctx.message.guild.id == config.utopia and ctx.channel.id == config.commands_channel: return True
    if (await database.get_staff(ctx.message.guild.id)) in [role.name for role in ctx.message.author.roles]: return True
    if ctx.message.author.id == ctx.message.guild.owner_id: return True

def dev_check(ctx):
    if ctx.message.author.id in [344404945359077377, 260479866921549845]: return True

def server_owner(ctx):
    if ctx.message.author.id == ctx.message.guild.owner_id: return True
